from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import Pergunta, Opcao



class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'ultimas_perguntas'
    def get_queryset(self):
        return Pergunta.objects.filter(data_de_publicacao__lte=timezone.now()
        ).order_by('-data_de_publicacao')[:5]

class DetalhesView(generic.DetailView):
    model = Pergunta
    template_name = 'polls/detalhes.html'
    def get_queryset(self):
        return Pergunta.objects.filter(data_de_publicacao__lte = timezone.now())


class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'polls/resultado.html'

def votacao (request, id_enquete):
    pergunta = get_object_or_404(Pergunta, pk = id_enquete)
    try:
        opcao_selecionada = pergunta.opcao_set.get(pk=request.POST['opcao'])
    except (KeyError, Opcao.DoesNotExist):
        return render(request, 'polls/detalhes.html',{
            'pergunta' : pergunta,
            'error_message' : "Selecione uma opção VÁLIDA!"
            })
    else:
        opcao_selecionada.votos +=1
        opcao_selecionada.save()
        return HttpResponseRedirect(
            reverse('polls:resultado', args=(pergunta.id,))
            )


